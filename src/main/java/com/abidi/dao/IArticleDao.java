package com.abidi.dao;
import com.abidi.gestion.entities.Article;

public interface IArticleDao extends IGenericDao<Article>{

}
